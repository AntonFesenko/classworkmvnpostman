package com.example.service.impl;

import com.example.data.dao.ManufacturerDao;
import com.example.data.dao.VehicleDao;
import com.example.data.entity.Manufacturer;
import com.example.data.entity.Vehicle;
import com.example.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {

    private final VehicleDao vehicleDao;
    private final ManufacturerDao manufacturerDao;

    @Autowired
    public VehicleServiceImpl(VehicleDao vehicleDao, ManufacturerDao manufacturerDao) {
        this.vehicleDao = vehicleDao;
        this.manufacturerDao = manufacturerDao;
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        return this.vehicleDao.findAll();
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        return vehicleDao.save(vehicle);
    }

    @Override
    public void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle) {
        Manufacturer manufacturer = manufacturerDao.findById(manufacturerId);
        manufacturer.getVehicles().add(vehicle);
        vehicle.setManufacturer(manufacturer);
        manufacturerDao.save(manufacturer);
    }

    @Override
    public Vehicle updateVehicle(Long manufacturerId, Vehicle vehicle) {
        Manufacturer manufacturer = manufacturerDao.findById(manufacturerId);
        vehicle.setManufacturer(manufacturer);
        return vehicleDao.update(vehicle);
    }

    @Override
    public Vehicle findById(Long id) {
        return vehicleDao.findById(id);
    }

    @Override
    public void delete(Long id) {
        vehicleDao.delete(id);
    }

    @Override
    public void update(Long id, Vehicle vehicle) {
        Vehicle vehicleUp = vehicleDao.findById(id);
    }


}
