package com.example.data.dao;

import com.example.data.entity.Vehicle;

import java.util.List;

public interface VehicleDao {

    public List findAll();

    public Vehicle save(Vehicle vehicle);

    public void delete(Long id);

    public Vehicle findById(Long id);

    public Vehicle update(Vehicle vehicle);
}

