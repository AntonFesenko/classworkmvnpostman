package com.example.data.dao.impl;

import com.example.data.dao.ManufacturerDao;
import com.example.data.entity.Manufacturer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ManufacturerDaoImpl implements ManufacturerDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Manufacturer findById(Long id) {
        return (Manufacturer) em.createQuery("SELECT m FROM Manufacturer m where m.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        em.persist(manufacturer);
        return manufacturer;
    }

    @Override
    public List<Manufacturer> findAll() {
        return em.createQuery("SELECT m from Manufacturer m")
                .getResultList();
    }

    @Override
     public Manufacturer findIdByCompanyName(String companyName) {
        return (Manufacturer) em.createQuery("SELECT m FROM Manufacturer m where m.company_name = :companyName")
                .setParameter("companyName", companyName)
                .getSingleResult();
    }
}