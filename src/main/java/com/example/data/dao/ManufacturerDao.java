package com.example.data.dao;


import com.example.data.entity.Manufacturer;

import java.util.List;

public interface ManufacturerDao {

    public Manufacturer findById(Long id);

    public Manufacturer save(Manufacturer manufacturer);

    public List<Manufacturer> findAll();

    public Manufacturer findIdByCompanyName(String companyName);

    }
